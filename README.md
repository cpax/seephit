# README #

SeePhit is a DSL for C++17 that allows you to write plain HTML strings in your C++ code and have them parsed at compile time into a tree like data structure.

It uses the very powerful constexpr features in C++17 to make your "stringly typed" HTML actually "strongly typed" and checkable at compiletime 

It provides fairly readable compile time error reporting of malformed syntax in both gcc and clang 


### How do I get set up? ###

Study the code and then


```g++ --std=c++17 main.cpp```

or

```clang++ --std=c++1x main.cpp```
 
	
	
It's a work in progress